STAGE DEPENDENCIES:
  Stage-1 is a root stage
  Stage-0 depends on stages: Stage-1

STAGE PLANS:
  Stage: Stage-1
    Map Reduce
      Map Operator Tree:
          TableScan
            alias: h
            filterExpr: (dt = '2018-05-07') (type: boolean)
            Statistics: Num rows: 10000000 Data size: 12340000000 Basic stats: COMPLETE Column stats: NONE
            Reduce Output Operator
              key expressions: id_1 (type: string)
              sort order: +
              Map-reduce partition columns: id_1 (type: string)
              Statistics: Num rows: 10000000 Data size: 12340000000 Basic stats: COMPLETE Column stats: NONE
              value expressions: id_2 (type: string), id_3 (type: string), id_4 (type: string), id_5 (type: string), id_6 (type: string), id_7 (type: string), id_8 (type: string), id_9 (type: string), change_code (type: string), s_date (type: string), e_date (type: string)
          TableScan
            alias: s
            Statistics: Num rows: 30000003 Data size: 27964090716 Basic stats: COMPLETE Column stats: PARTIAL
            Reduce Output Operator
              key expressions: id_1 (type: string)
              sort order: +
              Map-reduce partition columns: id_1 (type: string)
              Statistics: Num rows: 30000003 Data size: 27964090716 Basic stats: COMPLETE Column stats: PARTIAL
              value expressions: id_2 (type: string), id_3 (type: string), id_4 (type: string), id_5 (type: string), id_6 (type: string), id_7 (type: string), id_8 (type: string), id_9 (type: string), dt (type: string)
      Reduce Operator Tree:
        Join Operator
          condition map:
               Left Outer Join0 to 1
          keys:
            0 id_1 (type: string)
            1 id_1 (type: string)
          outputColumnNames: _col0, _col1, _col2, _col3, _col4, _col5, _col6, _col7, _col8, _col9, _col10, _col11, _col16, _col17, _col18, _col19, _col20, _col21, _col22, _col23, _col24, _col25
          Statistics: Num rows: 33000004 Data size: 30760500454 Basic stats: COMPLETE Column stats: NONE
          Filter Operator
            predicate: (_col25 = '2018-05-07') (type: boolean)
            Statistics: Num rows: 16500002 Data size: 15380250227 Basic stats: COMPLETE Column stats: NONE
            Select Operator
              expressions: _col0 (type: string), _col1 (type: string), _col2 (type: string), _col3 (type: string), _col4 (type: string), _col5 (type: string), _col6 (type: string), _col7 (type: string), _col8 (type: string), _col9 (type: string), _col10 (type: string), _col11 (type: string), '2018-05-07' (type: string), _col16 (type: string), _col17 (type: string), _col18 (type: string), _col19 (type: string), _col20 (type: string), _col21 (type: string), _col22 (type: string), _col23 (type: string), _col24 (type: string), '2018-05-07' (type: string)
              outputColumnNames: _col0, _col1, _col2, _col3, _col4, _col5, _col6, _col7, _col8, _col9, _col10, _col11, _col12, _col13, _col14, _col15, _col16, _col17, _col18, _col19, _col20, _col21, _col22
              Statistics: Num rows: 16500002 Data size: 15380250227 Basic stats: COMPLETE Column stats: NONE
              File Output Operator
                compressed: false
                Statistics: Num rows: 16500002 Data size: 15380250227 Basic stats: COMPLETE Column stats: NONE
                table:
                    input format: org.apache.hadoop.mapred.TextInputFormat
                    output format: org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat
                    serde: org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe

  Stage: Stage-0
    Fetch Operator
      limit: -1
      Processor Tree:
        ListSink

Time taken: 0.924 seconds, Fetched: 57 row(s)


